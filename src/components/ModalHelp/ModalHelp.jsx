import React from 'react';
import style from './ModalHelp.module.css'
import img from '../../assets/img/Wrapper.png'

const ModalHelp = ({openWindow,setOpenWindow}) => {
    const closeModal = () =>{
        setOpenWindow(false)
    }
    return (
        <div className={openWindow === false ? style.hide_modal : style.modal}>
            <img onClick={closeModal} className={style.img} src={img} alt=""/>
           <h1 className={style.title}>Как играть?</h1>
            <div className={style.text}>Свайпайте вправо, если видите выгодное решение. Свайпайте влево, если на карточке — не профит. Пройдите тест до конца и узнайте, умеете ли вы находить профит как профи.</div>
        </div>
    );
};

export default ModalHelp;