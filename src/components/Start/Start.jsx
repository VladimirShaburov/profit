import React from 'react';
import style from './Start.module.css'
import img from '../../assets/img/onboarding.webp'
import img_png from "../../assets/img/onboarding.png";
const Start = ({showStart,setShowStart}) => {
    const onclickBtn = () =>{
        setShowStart(true)
    }
    return (
        <div className={showStart ? style.hide : style.start}>
            <picture >
                <source srcSet={img} type="image/webp" />
                <source srcSet={img_png} type="image/png" />
                <img  className={style.img}  src={img_png} alt={""} />
            </picture>

            <div className={style.back}>
            <h1 className={style.title}>Правила просты</h1>
            <div className={style.text}>Попробуйте угадать, что выгодно, а что — не очень. Свайп вправо — профит, свайп влево — нет. Поехали.</div>
            </div>
            <button onClick={onclickBtn} className={style.button}><span>Начать</span></button>

        </div>
    );
};

export default Start;