import React from 'react';
import {Route, Routes} from "react-router-dom";
import App from "../App/App";
import Error from "../404/Error";

const Content = () => {
    return (
        <Routes>
            <Route path={'*'} element={<Error/>}/>
            <Route path={'/'} element={<App/>}/>
        </Routes>
    );
};

export default Content;