import React, { useMemo, useRef, useState} from 'react';
import Card from "../Card/Card";
import NavigationBar from "../NavigationBar/NavigationBar";
import style from './App.module.css'
import TinderCard from "react-tinder-card";
import card from "../../data/data";
import ModalHelp from "../ModalHelp/ModalHelp";
import Entrace from "../Entrace/Entrace";
import Start from "../Start/Start";
import CardProfit from "../Card/CardProfit";
import CardNoProfit from "../Card/CardNoProfit";
import Result from "../Result/Result";

const App = () => {

    const [openWindow, setOpenWindow] = useState(false)
    const [showEntrace, setShowEntrace] = useState(false)
    const [showStart, setShowStart] = useState(false)
    const [directionSwiped, setDirectionSwiped] = useState('')
    const [currentCard, setCurrentCard] = useState(false)
    const [showResult, setShowResult] = useState(false)
    const [counter, setCounter] = useState(20)
    const [profitVariants, setProfitVariants] = useState(0)
    const [currentIndex, setCurrentIndex] = useState(card.length - 1)
    const currentIndexRef = useRef(currentIndex)

    const openHelpWindow = () => {
        setOpenWindow(true)
    }

    const onSwipe = (direction, index, value) => {
        console.log('You swiped: ' + direction)
        setDirectionSwiped(direction)
        updateCurrentIndex(index - 1)
        setCounter(index)
        if (currentCard) {
            setDirectionSwiped('')
            setCurrentCard(false)
            updateCurrentIndex(index - 1)
        } else {
            updateCurrentIndex(index)
        }
        if (index === 0 && counter === 0) {
            setShowResult(true)
        }
        if (direction === value.true_answer) {
            setProfitVariants(profitVariants + 1)
        }
    }

    const childRefs = useMemo(
        () =>
            Array(card.length)
                .fill(0)
                .map((i) => React.createRef()),
        []
    )

    const updateCurrentIndex = (val) => {
        setCurrentIndex(val)
        currentIndexRef.current = val
    }

    const canSwipe = currentIndex >= 0

    const swipe = async (dir) => {

        if (canSwipe && currentIndex < card.length) {
            await childRefs[currentIndex].current.swipe(dir) // Swipe the card!
        }
    }

    const swiped = async (dir) => {
        if (canSwipe && currentIndex < card.length) {
            await childRefs[currentIndex].current.swipe(dir) // Swipe the card!
        }

    }

    return (

        <div className={style.app}>
            <div className={openWindow ? style.blind : style.blind_hide}>
                <NavigationBar
                    showEntrace={showStart}
                    openHelpWindow={openHelpWindow}
                    openWindow={openWindow}
                />

                <Entrace
                    showEntrace={showEntrace}
                    setShowEntrace={setShowEntrace}/>
                {showEntrace
                    ? <Start
                        showStart={showStart}
                        setShowStart={setShowStart}/>
                    : ''}
                {showStart && !showResult ?
                    <div className={openWindow ? style.container_hide : style.container}>
                        {card.map((value, index) => {
                            return (
                                <React.Fragment key={value.title}>
                                    <TinderCard ref={childRefs[index]}
                                                onSwipe={(direction) => onSwipe(direction, index, value)}
                                                preventSwipe={['up', 'down']} className={style.swipe}>
                                        <Card counter={counter}
                                              direction={directionSwiped}
                                              index={index}
                                              value={value}/>
                                    </TinderCard>
                                    {counter === index ?
                                        <>
                                            {directionSwiped === 'right' ?
                                                <CardProfit
                                                    setCurrentCard={setCurrentCard}
                                                    direction={directionSwiped}
                                                    index={index}
                                                    value={value}/>
                                                : ''}
                                            {directionSwiped === 'left' ? <CardNoProfit setCurrentCard={setCurrentCard}
                                                                                        direction={directionSwiped}
                                                                                        index={index} value={value}/>
                                                : ''}
                                        </>
                                        : ''}
                                </React.Fragment>
                            )
                        })}
                        <div>
                        </div>
                        <div className={style.btn}>
                            {directionSwiped === '' ?
                                <>
                                    <button onClick={() => swipe('left')} className={style.btn_no_profit}><span>Не профит</span>
                                    </button>
                                    <button onClick={() => swipe('right')} className={style.btn_profit}>
                                        <span>Профит</span></button>
                                </>
                                : ''}
                        </div>
                        {directionSwiped !== '' ?
                            <button onClick={() => swiped('top')} className={style.btn_next}><span>Далее</span>
                            </button> : ''}
                    </div>
                    : ''}
                {showResult ? <Result profitVariants={profitVariants}/> : ''}
            </div>

            <div>
                <ModalHelp setOpenWindow={setOpenWindow} openWindow={openWindow}/>
            </div>
        </div>

    );
};

export default App;