import React, {useEffect} from 'react';
import style from '../Card/Card.module.css'

const CardProfit = ({value,direction,setCurrentCard}) => {
useEffect(()=>{
    if(direction === 'right'){
        setCurrentCard(true)
    }
},[direction])
    return (
        <div className={style.card}>
            <div className={style.card_content}>
                <div className={style.card_background}></div>
            <div className={style.card_title_profit}>

                <div dangerouslySetInnerHTML={{__html : `${value.profit_variant}`}} className={style.card_text_profit}></div>
            </div>
                <div>
            <img  className={style.card_img} src={require(`../../assets/img/${value.img}.png`)} alt=""/>
                </div>
            </div>
        </div>
    );
};

export default CardProfit;