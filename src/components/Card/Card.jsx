import React from 'react';
import style from '../Card/Card.module.css'

const Card = ({value,index,counter}) => {

    return (
        <div className={style.card}>
            <div className={style.card_content}>
                <div className={style.card_background}></div>
            <div className={counter === index+1 ? style.card_title : style.card_hide}>
                 <div className={style.card_counter}>{value.id + 1}/20</div>
                <div dangerouslySetInnerHTML={{__html : `${value.title}`}} className={style.card_text}></div>
            </div>
                <div className={counter === index+1 ? '' : style.card_hide} >
            <img  className={style.card_img} src={require(`../../assets/img/${value.img}.png`)} alt=""/>
                </div>
            </div>
        </div>
    );
};

export default Card;