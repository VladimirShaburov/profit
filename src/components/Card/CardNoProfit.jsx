import React, {useEffect} from 'react';
import style from '../Card/Card.module.css'

const CardNoProfit = ({value,direction,setCurrentCard}) => {

    useEffect(()=>{
        if(direction === 'left'){
            setCurrentCard(true)
        }
    },[direction])

    return (

        <div className={style.card}>
            <div className={style.card_content}>
                <div  className={style.card_background}></div>
            <div className={style.card_title}>

                <div dangerouslySetInnerHTML={{__html : `${value.no_profit_variants}`}} className={style.card_text}></div>
            </div>
                <div>
            <img  className={style.card_img} src={require(`../../assets/img/${value.img}.png`)} alt=""/>
                </div>
            </div>
        </div>
    );
};

export default CardNoProfit;