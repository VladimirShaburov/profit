import React from 'react';
import img from '../../assets/img/end.webp'
import style from './Result.module.css'
import logo from '../../assets/img/yandex.png'
import img_png from "../../assets/img/end.png";
const Result = ({profitVariants}) => {
    return (
        <div className={style.container}>
            <h1 className={style.title}>Ваш результат: {profitVariants}/20</h1>
            <div className={style.text}>{
                profitVariants >= 0 && profitVariants < 7
                    ?
                    'Вы философ, для которого деньги — просто цветные бумажки. Понимаем, духовное — это важно, но кешбэк приятнее получать в виде реальных вещей. Например, в виде орешков со сгущенкой из Лавки за дополнительный кешбэк с оплаты покупок через Яндекс Пэй.'
                : ''}
                { profitVariants >= 7 && profitVariants < 10
                ?
                'Если учёные придумают «выгодометр», он сразу попадет в вашу корзину на маркетплейсе. Вместе с «измерителем диаметра пиццы» и «взбивателем финансовых подушек». А пока попробуйте Яндекс Пэй и получайте дополнительный кешбэк с каждой покупки.'
                : ''}
                { profitVariants >= 10 && profitVariants < 14
                    ?
                    'Вы берёте от жизни всё и на выгодных условиях. Если хочется плюшку из соседней пекарни, то со скидкой после 20:00. Вышел новый сериал? Подписка на стриминг шерится с друзьями. Ну а если покупать подарки на Новый год, то через Яндекс Пэй, чтобы получать ещё больше кешбэка с каждой оплаты.'
                    : ''}
                { profitVariants >= 14 && profitVariants < 20
                    ?
                    'Вас уже зовут преподавать финансовую грамотность? Наверняка сова с приглашением где-то рядом, ведь вы легко отличаете, что профит, а что нет.  Например, дополонительный кешбэк за покупки через Яндекс Пэй — это профит. Но вы и так знаете.'
                    : ''}
                { profitVariants === 20
                    ?
                    'Профит — ваше второе имя. Вы знаете, как вести личный бюджет и где самые дешёвые помидоры на районе. А чтобы получать ещё больше выгоды с каждой покупки, скачивайте Яндекс Пэй.'
                    : ''}
            </div>
            <button  className={style.button}><span>Скачать  <img  src={logo} alt=""/></span></button>
            <picture  className={style.img}  >
                <source srcSet={img} type="image/webp" />
                <source srcSet={img_png} type="image/png" />
                <img  className={style.img}  src={img_png} alt={""} />
            </picture>

        </div>
    );
};

export default Result;