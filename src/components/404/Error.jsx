import React from 'react';
import style from '../404/404.module.css'
import img from '../../assets/img/404.png'
import logo from '../../assets/img/Logo.png'
import {Link} from "react-router-dom";
const error = () => {
    return (
        <div className={style.error}>
            <div style={{display:"flex",width:'100%'}}>
            <img className={style.logo} width={77} height={28} src={logo} alt=""/>
            </div>
            <img width={297} height={309} className={style.img} src={img} alt=""/>
            <h1 className={style.title}>404</h1>
            <div className={style.text}>Упс. Что-то пошло не так</div>
            <Link className={style.link} to={'/'}>
            <button className={style.button}><span>На главную</span> </button>
            </Link>
        </div>
    );
};

export default error;