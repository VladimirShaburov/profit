import React from 'react';
import img_png from '../../assets/img/main_screen.png'
import img from '../../assets/img/main_screen.webp'
import style from './Entrace.module.css'

const Entrace = ({setShowEntrace,showEntrace}) => {

    const onclickBtn = (e) =>{
        e.preventDefault()
        setShowEntrace(true)

    }
    return (
        <div className={ showEntrace ? style.hide :style.entrace}>
            <picture className={style.img}>
                <source srcSet={img} type="image/webp" />
                <source srcSet={img_png} type="image/png" />
                <img  className={style.img}  src={img_png} alt={""} />
            </picture>
            {/*<img width={375}  className={style.img} src={img} alt=""/>*/}
            <h1 className={style.title}>Всё на пользу</h1>
            <div className={style.text}>Пройдите тест до конца и узнайте, умеете ли вы находить выгоду как профи. Ну или как Мистер Профит</div>
            <button onClick={onclickBtn} className={style.button}><span>Играть</span></button>
        </div>
    );
};

export default Entrace;