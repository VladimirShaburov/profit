import React from 'react';
import style from './NavigationBar.module.css'
import logo from '../../assets/img/Logo.png'
import help from '../../assets/img/help.png'

const NavigationBar = ({openHelpWindow,openWindow,showEntrace}) => {

    return (
        <div className={style.navigation_bar}>
            <img  src={logo} alt=""/>
            {showEntrace ? <img onClick={openHelpWindow} className={style.img} src={help} alt=""/> : '' }

        </div>
    );
};

export default NavigationBar;