import React from 'react';
import ReactDOM from 'react-dom/client';
import './assets/styles/global.css'
import {BrowserRouter} from "react-router-dom";
import Content from "./components/Content/Content";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
<BrowserRouter>
    <Content />
</BrowserRouter>
);

